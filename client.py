import socket
import threading
import time
import random

HEADER_SIZE = 64
HOST = '192.168.42.33'
PORT = 9999    
jokes_question = ["Do you want to know a joke?", "Hear that?", "If I promise to miss you..."]
jokes_answer = ["You", "it's the sound of you not talking for once", " will you go, like, really far away?"]

def send(msg):
    message = msg.encode('utf-8')
    client.send(message)
    
def real_send(msg):
    msg_len = len(msg)
    msg_len = (HEADER_SIZE - msg_len)*' ' + str(msg_len)
    send(msg_len)
    send(msg)

def execute_job():
    global connected, break_alphabet_status, break_jokes_status
    while connected :
        msg = client.recv(2048).decode('utf-8')
        if msg == "break job alphabet\r" :
            break_alphabet_status = True
        elif msg == "break job jokes\r" :
            break_jokes_status = True
        manage_input(msg)

def manage_input(msg):
    global connected, break_alphabet_status, break_jokes_status
    if msg == "close client":
        print("Connection to client has been terminated")
        client.close()
        time.sleep(1)
        connected = False
    elif msg == "count":
        for i in range(65, 91) :
            if break_alphabet_status :
                break
            real_send(chr(i))
            time.sleep(1)
        if break_alphabet_status :
            real_send("alphabet work has failed #Status")
            break_alphabet_status = False
        else :
            real_send("alphabet work is done #Status") 
            break_alphabet_status = False
    elif msg == "getjokes":
        random_idx = random.randint(0,len(jokes_question)-1)
        real_send(jokes_question[random_idx])
        for i in range(5) :
            if break_jokes_status :
                break
            time.sleep(1)
        if not break_jokes_status :
            real_send(jokes_answer[random_idx])
            time.sleep(1)
            real_send("jokes work is done #Status")
        elif break_jokes_status :
            real_send("jokes work has failed #Status")
            break_jokes_status = False

if __name__ == "__main__":
    global connected, break_alphabet_status, break_jokes_status
    connected = True
    break_alphabet_status = False
    break_jokes_status = False
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect((HOST, PORT))
        thread1 = threading.Thread(target = execute_job)
        thread2 = threading.Thread(target = execute_job)
        thread1.start()
        thread2.start()
        while connected :
            msg = input()
            real_send(msg)