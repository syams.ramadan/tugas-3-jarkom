import socket
import threading
import math
import msvcrt
import time
import os
import sys

HEADER_SIZE = 64 
HOST = socket.gethostbyname(socket.gethostname())
PORT = 9999 
status = []
downscale_counter = []
continue_client = []
status_job = []
job_client = []
running_client = []
conn_running_job = []
inputable_char = "abcdefghijklmnopqrstuvwxyz\r1234567890 \x08"

def get_job_idx(job_string):
    if job_string == "alphabet" :
        return 0
    elif job_string == "jokes" :
        return 1

def input_with_timeout(client_idx):
    result = []
    result_str = ""
    while continue_client[client_idx]:
        if len(running_client) != 0 and running_client[-1] == ("client " + str(client_idx)): 
            if msvcrt.kbhit():
                input_char = msvcrt.getwch()
                if input_char == "\x08":
                    space = " " * len(result_str)
                    sys.stdout.write("\b" * len(result_str))
                    sys.stdout.flush()
                    sys.stdout.write(space)
                    sys.stdout.flush()
                    sys.stdout.write("\b" * len(result_str))
                    sys.stdout.flush()
                    result_str = result_str[:-1]
                    sys.stdout.write(result_str)
                    sys.stdout.flush()
                elif input_char in inputable_char:
                    sys.stdout.write(input_char)
                    sys.stdout.flush()
                    result.append(input_char)
                    result_str += input_char
                if len(result) != 0 and result[-1] == '\r':
                    print()
                    return result_str
        time.sleep(0.04)
    return "client has been closed"
        
def handle_client(conn, addr, client_idx):
    print(f"[NEW CONNECTION] {addr} connected.")
    connected = True
    while connected:
        try :
            msg_len = conn.recv(HEADER_SIZE).decode('utf-8')
            if msg_len:
                msg_len = int(msg_len)
                msg = conn.recv(msg_len).decode('utf-8')
                if "#Status" not in msg :
                    print(f"client {client_idx} : {msg}")
                    conn.send(f"message of length {msg_len} recieved: {msg}".encode('utf-8'))
                if "work is done" in msg :
                    status[client_idx] = "Idle"
                    job_idx = get_job_idx(msg.split(" ")[0])
                    status_job[job_idx] = "Finished"
                    job_client[job_idx] = "N/A"
                    running_client.append("client " + str(client_idx))
                elif "work has failed" in msg :
                    job_idx = get_job_idx(msg.split(" ")[0])
                    status[client_idx] = "Idle"
                    status_job[job_idx] = "Failed"
                    job_client[job_idx] = "N/A"
                    running_client.append("client " + str(client_idx))
        except :
            connected = False
    conn.close()

def close_client(conn, addr, client_idx):
    conn.send("close client".encode('utf-8'))
    continue_client[client_idx] = False
    status[client_idx] = "Dead"
    time.sleep(1)
    running_client.remove("client " + str(client_idx))
    print(f"Connection to client {client_idx} has been closed")
    print(f"[ACTIVE CONNECTIONS] {math.floor(threading.activeCount()/3)}")
    conn.close()

def execute_client(conn, addr, client_idx):
    while continue_client[client_idx]:
        command = input_with_timeout(client_idx).lower()
        if command == "close client\r":
            close_client(conn, addr, client_idx)
        elif command == "list the alphabet\r" and status_job[0] != "Running" and status[client_idx] != "Running":
            conn.send("count".encode('utf-8'))
            status[client_idx] = "Running"
            if len(running_client) > 1 : running_client.pop()
            conn_running_job[0] = conn
            downscale_counter[client_idx] = 0
            status_job[0] = "Running"
            job_client[0] = "Client " + str(client_idx)
        elif command == "give me a joke\r" and status_job[1] != "Running" and status[client_idx] != "Running":
            conn.send("getjokes".encode('utf-8'))
            status[client_idx] = "Running"
            if len(running_client) > 1 : running_client.pop()
            conn_running_job[1] = conn
            downscale_counter[client_idx] = 0
            status_job[1] = "Running"
            job_client[1] = "Client " + str(client_idx)
        elif "get status of client" in command :
            input_idx = int(command.split(" ")[4])
            print(status[input_idx])
        elif "get status of job" in command :
            input_idx = get_job_idx(command.split(" ")[4][:-1])
            print(status_job[input_idx])
        elif "job's client" in command :
            input_idx = get_job_idx(command.split(" ")[1])
            print(job_client[input_idx])
        elif "break job" in command :
            input_idx = get_job_idx(command.split(" ")[2][:-1])
            conn_running_job[input_idx].send(command.encode('utf-8'))

def start():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.bind((HOST, PORT))
        server.listen(2)
        print(f"[LISTENING] Server is listening on {HOST}")
        for i in range(2):
            status_job.append("Ready to run")
            conn_running_job.append("Lol")
            job_client.append("N/A")
        i = 0
        while True:
            conn, addr = server.accept()
            status.append("Idle")
            downscale_counter.append(0)
            running_client.append("client " + str(i))
            continue_client.append(True)
            thread1 = threading.Thread(target = handle_client, args=(conn, addr, i))
            thread2 = threading.Thread(target = execute_client, args=(conn, addr, i))
            thread3 = threading.Thread(target = downscale, args=(conn, addr, i))
            thread1.start()
            thread2.start()
            thread3.start()
            print(f"[ACTIVE CONNECTIONS] {math.floor(threading.activeCount()/3)}")
            i += 1
            
def downscale(conn, addr, client_idx):
    while continue_client[client_idx] :
        if status[client_idx] == "Idle":
            downscale_counter[client_idx] += 1
            if downscale_counter[client_idx] == 300 :
                close_client(conn, addr, client_idx)
            time.sleep(1)

if __name__ == "__main__":
    print("Server is starting")
    start()